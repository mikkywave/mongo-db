> db.inventory.deleteMany({"qty": {$gt: 99}})
{ "acknowledged" : true, "deletedCount" : 2 }

> db.inventory.deleteOne({"item": "planner"})
{ "acknowledged" : true, "deletedCount" : 1 }

> db.inventory.deleteMany({"tags": {$in: ["blue"]}})
{ "acknowledged" : true, "deletedCount" : 1 }

> db.inventory.deleteMany({})
{ "acknowledged" : true, "deletedCount" : 6 }

> db.inventory.find()
